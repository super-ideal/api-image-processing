const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const bodyParser = require('body-parser');

// Configure server express
const app = express();
const server = require("http").Server(app);

// Configure Helmet
app.use(helmet());

// Configure app to use Cors
app.use(cors({
  origin: '*',
  optionsSuccessStatus: 200,
  methods: ['GET', 'PUT', 'POST', 'DELETE'],
  allowedHeaders: ['Content-Type', 'X-Requested-With'],
  credentials: false
}));

// Configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Set port server
app.set('port', process.env.PORT || 4000);

// Routes
const router = require('./src/app/routes')(app);

// Start Express server
server.listen(app.get('port'), function () {
  console.log('Servidor rodando na porta ' + app.get('port') + ' em modo ' + app.get('env'));
});

module.exports = app;
