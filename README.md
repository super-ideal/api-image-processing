Upload and Processing Images
=====================

High performance Node.js image processing, the fastest module to resize JPEG, PNG, WebP and TIFF images. Uses the libvips library. http://sharp.pixelplumbing.com/

## Api Restful

Certifique-se que você tenha instalado o [NodeJs](https://nodejs.org/en/download/) em sua máquina.

Clone o projeto para sua máquina:
```bash
$ git clone <LINK DO REPOSITÓRIO>
```

Após ter o projeto em sua máquina, execute o comando abaixo no terminal para poder instalar as dependências necessárias:
```bash
$ npm install
```

Para rodar o servidor e testar a api, execute:
```bash
$ npm start
```

## Instalação do Sharp
Acessar o site [http://sharp.dimens.io/en/v0.11.4/install](http://sharp.dimens.io/en/v0.11.4/install) para ver como instalar o sharp.