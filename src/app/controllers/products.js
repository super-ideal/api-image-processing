'use strict';
const fs = require('fs-extra');
const multer = require('multer');
const path = require('path');

const sharpImage = require('../sharp/sharp-image');
const dirFile = './src/assets/uploads/images/products/';

exports.post = function (req, res, next) {
  const imageFilter = function (req, file, cb) {
    if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/)) {
      return cb(new Error('Only image files are allowed!'), false);
    }
    cb(null, true);
  };

  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, dirFile)
    },
    filename: function (req, file, cb) {
      cb(null, `${file.fieldname}-${Date.now()}${path.extname(file.originalname)}`);
    }
  });

  var upload = multer({
    storage: storage,
    fileFilter: imageFilter,
    limits: { fileSize: 1024 * 1024 }
  }).array('product', 12);

  upload(req, res, function (err) {
    if (err) {
      return res.status(415).json(err);
    }

    res.json(req.files);
  })
}

exports.get = function (req, res, next) {
  const pathFile = dirFile + req.params.filename;
  const format = req.query.format || path.extname(req.params.filename).substring(1) || 'png';

  // Verifica se o arquivo existe
  const exist = fs.existsSync(pathFile);
  if (!exist) {
    return res.status(404).json({ code: 'ENOENT', message: 'Arquivo não existe' });
  }

  // Set the content-type of the response
  res.type(`image/${format}`);

  // Get the resized image
  sharpImage({
    path: pathFile,
    format: format,
    quality: req.query.quality ? parseInt(req.query.quality) : null,
    width: req.query.width ? parseInt(req.query.width) : null,
    height: req.query.height ? parseInt(req.query.height) : null,
    stretch: req.query.stretch,
    max: req.query.max,
    min: req.query.min,
    embed: req.query.embed,
    crop: req.query.crop,
    rotate: req.query.rotate ? parseInt(req.query.rotate) : null,
    flip: req.query.flip,
    flop: req.query.flop,
    blur: req.query.blur ? parseInt(req.query.blur) : null,
    colorhex: req.query.colorhex,
    greyscale: req.query.greyscale,
  }).pipe(res);
}

exports.delete = function (req, res, next) {
  var path = './src/assets/uploads/images/products/' + req.params.filename;

  fs.remove(path, err => {
    if (err) {
      return res.status(404).json({ code: 'ENOENT', message: 'Arquivo não existe' });
    }

    return res.status(200).json({ message: 'Arquivo excluído com sucesso' });
  })
}
