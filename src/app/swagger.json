{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "Upload e manipulação de imagens",
    "description": "API de upload e manipulação de imagens"
  },
  "host": "localhost:8080",
  "basePath": "/upload/image/",
  "tags": [
    {
      "name": "Perfil",
      "description": "Imagens de perfil dos usuários"
    },
    {
      "name": "Produto",
      "description": "Imagens dos produtos"
    }
  ],
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/profile": {
      "post": {
        "tags": [
          "Perfil"
        ],
        "summary": "Envia uma imagem",
        "parameters": [
          {
            "name": "profile",
            "in": "formData",
            "type": "file",
            "description": "Imagem para upload"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Imagem carregada para o servidor.",
            "schema": {
              "$ref": "#/definitions/ImagePost200"
            }
          },
          "415": {
            "description": "Tipo de imagem não aceito ou imagem muito grande"
          }
        }
      }
    },
    "/profile/{filename}": {
      "parameters": [
        {
          "name": "filename",
          "in": "path",
          "required": true,
          "description": "Nome do arquivo de imagem",
          "type": "string"
        }
      ],
      "get": {
        "tags": [
          "Perfil"
        ],
        "summary": "Busca uma imagem",
        "parameters": [
          {
            "name": "format",
            "in": "query",
            "description": "Formato de imagem caso queira converter (jpg, jpeg, giff, png, webp, tiff)",
            "type": "string"
          },
          {
            "name": "quality",
            "in": "query",
            "description": "Qualidade da imagem de 1 a 100, sendo 1 muito ruim e 100 ótima qualidade",
            "type": "number"
          },
          {
            "name": "width",
            "in": "query",
            "description": "Tamanho da largura para redimencionar ou cortar a imagem",
            "type": "number"
          },
          {
            "name": "height",
            "in": "query",
            "description": "Tamanho da altura para redimencionar ou cortar a imagem",
            "type": "number"
          },
          {
            "name": "stretch",
            "in": "query",
            "description": "Ignora a proporção da entrada e estica a imagem até a largura e/ou altura exatas fornecidas",
            "type": "boolean",
            "default": false
          },
          {
            "name": "max",
            "in": "query",
            "description": "Preserva a proporção e redimensiona a imagem para ser o maior possível, garantindo que suas dimensões sejam menores ou iguais à largura e altura especificadas",
            "type": "boolean",
            "default": false
          },
          {
            "name": "min",
            "in": "query",
            "description": "Preserva a proporção e redimensiona a imagem para o menor tamanho possível, garantindo que suas dimensões sejam maiores ou iguais à largura e altura especificadas",
            "type": "boolean",
            "default": false
          },
          {
            "name": "embed",
            "in": "query",
            "description": "Redimenciona a imagem sem cortá-la, mas adiciona um plano de fundo na imagem",
            "type": "boolean",
            "default": false
          },
          {
            "name": "crop",
            "in": "query",
            "description": "Corta a imagem de acordo com a largura e altura informada",
            "type": "boolean",
            "default": false
          },
          {
            "name": "rotate",
            "in": "query",
            "description": "Rotaciona a imagem para os seguintes ângulos a ser informados: 90,180,270,-90,-180,-270",
            "type": "number",
            "default": null
          },
          {
            "name": "flip",
            "in": "query",
            "description": "Inverte a imagem verticalmente",
            "type": "boolean",
            "default": false
          },
          {
            "name": "flop",
            "in": "query",
            "description": "Inverte a imagem horizontalmente",
            "type": "boolean",
            "default": false
          },
          {
            "name": "blur",
            "in": "query",
            "description": "Aplica efeito de desfoque sobre a imagem",
            "type": "number",
            "default": null
          },
          {
            "name": "colorhex",
            "in": "query",
            "description": "Informa uma cor para aplicar no fundo da imagem",
            "type": "string",
            "default": null
          },
          {
            "name": "greyscale",
            "in": "query",
            "description": "Aplica o efeito de tons de cinza sobre a imagem",
            "type": "boolean",
            "default": false
          }
        ],
        "responses": {
          "200": {
            "description": "Imagem encontrada"
          },
          "404": {
            "description": "Imagem não encontrada",
            "schema": {
              "$ref": "#/definitions/Image404"
            }
          }
        }
      },
      "delete": {
        "summary": "Exclui uma imagem",
        "tags": [
          "Perfil"
        ],
        "responses": {
          "200": {
            "description": "Imagem excluída",
            "schema": {
              "$ref": "#/definitions/ImageDelete200"
            }
          },
          "404": {
            "description": "Imagem não encontrada",
            "schema": {
              "$ref": "#/definitions/Image404"
            }
          }
        }
      }
    },
    "/product": {
      "post": {
        "tags": [
          "Produto"
        ],
        "summary": "Envia uma imagem",
        "parameters": [
          {
            "name": "profile",
            "in": "formData",
            "type": "file",
            "description": "Imagem para upload"
          }
        ],
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Imagem carregada para o servidor",
            "schema": {
              "$ref": "#/definitions/ImagePost200"
            }
          },
          "415": {
            "description": "Tipo de imagem não aceito ou imagem muito grande"
          }
        }
      }
    },
    "/product/{filename}": {
      "parameters": [
        {
          "name": "filename",
          "in": "path",
          "required": true,
          "description": "Nome do arquivo de imagem",
          "type": "string"
        }
      ],
      "get": {
        "tags": [
          "Produto"
        ],
        "summary": "Busca uma imagem",
        "parameters": [
          {
            "name": "format",
            "in": "query",
            "description": "Formato de imagem caso queira converter (jpg, jpeg, giff, png, webp, tiff)",
            "type": "string"
          },
          {
            "name": "quality",
            "in": "query",
            "description": "Qualidade da imagem de 1 a 100, sendo 1 muito ruim e 100 ótima qualidade",
            "type": "number"
          },
          {
            "name": "width",
            "in": "query",
            "description": "Tamanho da largura para redimencionar ou cortar a imagem",
            "type": "number"
          },
          {
            "name": "height",
            "in": "query",
            "description": "Tamanho da altura para redimencionar ou cortar a imagem",
            "type": "number"
          },
          {
            "name": "stretch",
            "in": "query",
            "description": "Ignora a proporção da entrada e estica a imagem até a largura e/ou altura exatas fornecidas",
            "type": "boolean",
            "default": false
          },
          {
            "name": "max",
            "in": "query",
            "description": "Preserva a proporção e redimensiona a imagem para ser o maior possível, garantindo que suas dimensões sejam menores ou iguais à largura e altura especificadas",
            "type": "boolean",
            "default": false
          },
          {
            "name": "min",
            "in": "query",
            "description": "Preserva a proporção e redimensiona a imagem para o menor tamanho possível, garantindo que suas dimensões sejam maiores ou iguais à largura e altura especificadas",
            "type": "boolean",
            "default": false
          },
          {
            "name": "embed",
            "in": "query",
            "description": "Redimenciona a imagem sem cortá-la, mas adiciona um plano de fundo na imagem",
            "type": "boolean",
            "default": false
          },
          {
            "name": "crop",
            "in": "query",
            "description": "Corta a imagem de acordo com a largura e altura informada",
            "type": "boolean",
            "default": false
          },
          {
            "name": "rotate",
            "in": "query",
            "description": "Rotaciona a imagem para os seguintes ângulos a ser informados: 90,180,270,-90,-180,-270",
            "type": "number",
            "default": null
          },
          {
            "name": "flip",
            "in": "query",
            "description": "Inverte a imagem verticalmente",
            "type": "boolean",
            "default": false
          },
          {
            "name": "flop",
            "in": "query",
            "description": "Inverte a imagem horizontalmente",
            "type": "boolean",
            "default": false
          },
          {
            "name": "blur",
            "in": "query",
            "description": "Aplica efeito de desfoque sobre a imagem",
            "type": "number",
            "default": null
          },
          {
            "name": "colorhex",
            "in": "query",
            "description": "Informa uma cor para aplicar no fundo da imagem",
            "type": "string",
            "default": null
          },
          {
            "name": "greyscale",
            "in": "query",
            "description": "Aplica o efeito de tons de cinza sobre a imagem",
            "type": "boolean",
            "default": false
          }
        ],
        "responses": {
          "200": {
            "description": "Imagem encontrada"
          },
          "404": {
            "description": "Imagem não encontrada",
            "schema": {
              "$ref": "#/definitions/Image404"
            }
          }
        }
      },
      "delete": {
        "summary": "Exclui uma imagem",
        "tags": [
          "Produto"
        ],
        "responses": {
          "200": {
            "description": "Imagem excluída",
            "schema": {
              "$ref": "#/definitions/ImageDelete200"
            }
          },
          "404": {
            "description": "Imagem não encontrada",
            "schema": {
              "$ref": "#/definitions/Image404"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "Image404": {
      "properties": {
        "code": { "type": "string" },
        "message": { "type": "string" }
      }
    },
    "ImageDelete200": {
      "properties": {
        "message": { "type": "string" }
      }
    },
    "ImagePost200": {
      "properties": {
        "fieldname": { "type": "string" },
        "originalname": { "type": "string" },
        "encoding": { "type": "string" },
        "mimetype": { "type": "string" },
        "destination": { "type": "string" },
        "filename": { "type": "string" },
        "path": { "type": "string" },
        "size": { "type": "number" }
      }
    }
  }
}