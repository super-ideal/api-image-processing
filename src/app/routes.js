'use strict';
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const profilesController = require('./controllers/profiles');
const productsController = require('./controllers/products');

module.exports = function (app) {
  const up = express.Router();
  const image = express.Router();

  app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
  // app.use('/upload', up);

  app.use('/image',
    image.get('/profile/:filename', profilesController.get),
    image.post('/profile', profilesController.post),
    image.delete('/profile/:filename', profilesController.delete),

    image.get('/product/:filename', productsController.get),
    image.post('/product', productsController.post),
    image.delete('/product/:filename', productsController.delete),
  );
};