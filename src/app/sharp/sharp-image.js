const fs = require('fs-extra');
const sharp = require('sharp');

module.exports = function sharpImage(
  obj = {
    path: path,
    format: format,
    quality: quality,
    width: width,
    height: height,
    stretch: stretch,
    max: max,
    min: min,
    embed: embed,
    crop: crop,
    rotate: rotate,
    flip: flip,
    flop: flop,
    blur: blur,
    colorhex: colorhex,
    greyscale: greyscale
  }
) {
  const readStream = fs.createReadStream(obj.path);
  let transform = sharp();

  // Converte a imagem para um determinado formato (jpg, jpeg, png, gif, ...)
  if (obj.format) {
    transform = transform.toFormat(obj.format);
  }

  // Define a qualidade da imagem
  if (obj.quality && (obj.format === 'jpg' || obj.format === 'jpeg')) {
    transform = transform.jpeg({
      quality: obj.quality,
      chromaSubsampling: '4:4:4'
    })
  }

  if (obj.quality && obj.format === 'webp') {
    transform = transform.webp({
      quality: obj.quality,
      lossless: true
    })
  }

  if (obj.quality && obj.format === 'tiff') {
    transform = transform.tiff({
      quality: obj.quality
    })
  }

  // Redimensiona a imagem
  if (obj.width || obj.height) {
    transform = transform.resize(obj.width || null, obj.height || null);

    // Ignorando a proporção da entrada, estique a imagem até a largura e/ou altura exatas fornecidas
    if (obj.stretch) {
      transform = transform.ignoreAspectRatio()
    }

    // Redimenciona a imagem para o maior tamanho informado, sem cortar os lados
    if (obj.max) {
      transform = transform.max();
    }

    // Redimenciona a imagem para o menor tamanho informado, sem cortar os lados
    if (obj.min) {
      transform = transform.min();
    }

    // Redimenciona a imagem sem corta-la, mas adiciona um plano de fundo
    if (obj.embed) {
      transform = transform.embed('centre');
    }

    // Corta a imagem
    if (obj.crop) {
      transform = transform.crop(sharp.strategy.entropy);
    }
  }

  // Rotaciona a imagem
  if (obj.rotate && obj.rotate.toString().match(/(90|180|270|-90|-180|-270)/)) {
    transform = transform.rotate(obj.rotate);
  }

  // Inverte a imagem verticalmente
  if (obj.flip && JSON.parse(obj.flip)) {
    transform = transform.flip();
  }

  // Inverte a imagem horizontalmente
  if (obj.flop && JSON.parse(obj.flop)) {
    transform = transform.flop();
  }

  // Desfoca a imagem
  if (obj.blur) {
    transform = transform.blur(obj.blur);
  }

  // Define a cor de background da imagem
  if (obj.colorhex) {
    transform = transform.background('#' + obj.colorhex).flatten()
  }

  // Aplica escala cinzenta sobre a imagem
  if (obj.greyscale && JSON.parse(obj.greyscale)) {
    transform = transform.greyscale();
  }

  /*transform = transform.overlayWith('./src/assets/overlay.png', {
    gravity: sharp.gravity.southeast
  }).sharpen()*/

  return readStream.pipe(transform);
};